class User < ActiveRecord::Base
    validates :name, presence: true, uniqueness: true, 
        length: {minimum: 3, maximum: 255}
    validates :email, presence: true, uniqueness: true, 
        length: {minimum: 5, maximum: 255},
        format: {with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i}
    
    has_secure_password
end
