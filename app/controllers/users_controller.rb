class UsersController < ApplicationController
    # attr_accessor :rsa_public, :rsa_private
    @@rsa_public = OpenSSL::PKey::RSA.new(Base64::urlsafe_decode64(Rails.application.secrets.rsa_public_key))
    @@rsa_private = OpenSSL::PKey::RSA.new(Base64::urlsafe_decode64(Rails.application.secrets.rsa_private_key))

    def initialize

    end

    def new
        render json: {status: 'ok'}
    end

    def create
        user = User.new(user_signup_params)
        if user.save
            auth_token_1, auth_token_2 = public_encrypt_username_password(user.name, user_signup_params[:password])
            encoded1 = Base64.urlsafe_encode64(auth_token_1)#.encode('utf-8')
            encoded2 = Base64.urlsafe_encode64(auth_token_2)#.encode('utf-8')
            # prepare json
            res = {}
            res[:status] = 'ok'
            res[:name] = user.name
            res[:token1] = encoded1
            res[:token2] = encoded2
            render json: res.to_json
        else
            res = {}
            res[:status] = 'bad_request'
            res[:errors] = user.errors if user.errors.count > 0
            render json: res.to_json
        end
    end

    def get_token
        res = {}
        user = User.find_by(email: params[:user][:email])
        if user && user.authenticate(params[:user][:password])
            auth_token_1, auth_token_2 = public_encrypt_username_password(user.name, params[:user][:password])
            encoded1 = Base64.urlsafe_encode64(auth_token_1)#.encode('utf-8')
            encoded2 = Base64.urlsafe_encode64(auth_token_2)#.encode('utf-8')
            # prepare json
            res[:status] = 'ok'
            res[:name] = user.name
            res[:token1] = encoded1
            res[:token2] = encoded2
            render json: res.to_json
        else
            res[:status] = 'invalid_email_or_password'
            render json: res.to_json
        end
    end

    def auth
        user = self.class.authenticate_user(params[:token1], params[:token2])
        res = {}
        if user
            res[:name] = user.name
            res[:email] = user.email
        else
            res[:status] = 'authentication_error'
        end
        render json: res
    end

    def UsersController.authenticate_user(token1, token2)
        begin
            user_name = @@rsa_private.private_decrypt(Base64.urlsafe_decode64(token1))#.force_encoding('utf-8')
            password = @@rsa_private.private_decrypt(Base64.urlsafe_decode64(token2))#.force_encoding('utf-8')
            user = User.find_by(name: user_name)
        rescue
            return nil
        end
        if user && user.authenticate(password)
            return user
        else
            return nil
        end
    end

private
    def public_encrypt_username_password(name, password)
        token_1 = @@rsa_public.public_encrypt(name)
        token_2 = @@rsa_public.public_encrypt(password)
        return token_1, token_2
    end

    def user_signup_params
        params.require(:user).permit(:email, :name, :password, :password_confirmation)
    end 

    def user_login_params
        params.require(:user).permit(:email,:password)
    end 

end
