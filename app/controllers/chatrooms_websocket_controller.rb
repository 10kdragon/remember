class ChatroomsWebsocketController < WebsocketRails::BaseController
    def chatroom_message_receive
        msg = message()
        p msg

        if msg.include? 'ready'
            chatroom = msg['ready']['chatroom']
            if chatroom
                # check role (caller/callee)
                if ChatroomsController.verifyChatroomCode(chatroom, msg['ready']['code'])
                    send_message chatroom, {role: 'caller', nonce: msg['nonce']}
                else
                    send_message chatroom, {role: 'callee', nonce: msg['nonce']}
                end

                # check how many peers
                if controller_store.include? chatroom
                    controller_store[chatroom] += 1
                    if controller_store[chatroom] == 2
                        p 'broadcasting: connected.'
                        broadcast_message chatroom, {state: 'connected'}
                    end
                else
                    controller_store[chatroom] = 1
                end
            end
        end

        if msg.include?('chatroom') && msg.include?('code')
            chatroom = msg['chatroom']
            code = msg['code']
            role_name = ''

            if ChatroomsController.verifyChatroomCode(chatroom, code)
                role_name = 'caller'
            else
                role_name = 'callee'
            end

            if msg.include? 'ice'
                p 'broadcasting: ice to ' + chatroom
                broadcast_message chatroom, {ice: msg['ice'], role: role_name}
            end
            if msg.include? 'sdp'
                p 'broadcasting: sdp to ' + chatroom
                broadcast_message chatroom, {sdp: msg['sdp'], role: role_name}
            end
        end

        # send_message 'hii', msg
        # broadcast_message(:chatroom, msg)
    end

    # def new_chatroom_message_receive
    #     msg = message()
    #     p msg 
    #     # broadcast_message(:client_connected, msg)
    # end
end