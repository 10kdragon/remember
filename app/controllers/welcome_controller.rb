class WelcomeController < ApplicationController

  # GET /welcome
  def index
    render json: {status: 'ok'}
  end

end
