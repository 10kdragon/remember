class ChatroomsController < ApplicationController
    @@chatrooms = {} # id => code, code will be clear to 0 once room is fully initialized
    @@ice = {}
    @@sdp = {}

    def index
        res = {}
        res[:status] = 'ok'
        res[:chatrooms] = @@chatrooms
        render json: res.to_json
    end

    def new
        res = {}
        name = params[:id]
        if name.length < 3 || name.length > 127
            res[:status] = 'invalid'
        elsif @@chatrooms.include? name
            res[:status] = 'existed'
        else
            @@chatrooms[name] = (rand()*1000000000000000).to_i
            res[:status] = 'ok'
            res[:chatroom_id] = name
            res[:chatroom_code] = @@chatrooms[name];
        end
        render json: res.to_json
    end

    def self.verifyChatroomCode(name, code)
        code = code.to_i
        if name.empty? || name.length > 127 || !@@chatrooms.include?(name)
            return false
        elsif code == 0 || @@chatrooms[name] != code
            return false
        else # ok
            return true
        end
    end

    # def create
    #     res = {}
    #     name = params[:id]
    #     code = params[:code].to_i
    #     ice = params[:ice]
    #     sdp = params[:sdp]
    #     if name.empty? || name.length > 127 || !@@chatrooms.include?(name)
    #         res[:status] = 'invalid'
    #     elsif code == 0 || @@chatrooms[name] != code
    #         res[:status] = 'unauthorized'
    #     else # ok
    #         @@chatrooms[name] = 0
    #         @@ice[name] = ice
    #         @@sdp[name] = sdp
    #         res[:status] = 'ok'
    #     end
    #     render json: res.to_json
    # end

    def enter
        res = {}
        name = params[:id]
        if @@chatrooms.include? name
            res[:status] = 'ok'
            res[:chatroom_id] = name
        else
            res[:status] = 'not_found'
        end
        render json: res.to_json
    end
end
