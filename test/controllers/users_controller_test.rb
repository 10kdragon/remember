require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = User.new(name: "user01", email: "user+-01@test-1.com.sg",
             password: "password", password_confirmation: "password")
  end

  test "add new user" do
    assert_difference('User.count') do
      post :create, user: { email: @user.email, name: @user.name,
             password: "password", password_confirmation: "password" }
    end
  end
end
