# Remeber

# Install PostgreSQL
First install PG
```sh
sudo apt-get install postgresql postgresql-contrib
```

Setup a Role for local dev/test environment
```sh
sudo -i -u postgres
psql
CREATE ROLE rails_dev with CREATEDB LOGIN password 'rails_dev_123';
\q
```

Now do the rake commands
```sh
rake db:create
rake db:migrate
```

# To reset database tables
```sh
rake db:reset
```

# Fix 'gem pg' installation error
You need install the postgreSQL dev package with header of PostgreSQL by 
```sh
sudo apt-get install libpq-dev


# Generate RSA private and public keys for Production environment
```sh
# In Ruby console
require 'openssl'
require 'base64'
rsa = OpenSSL::PKey::RSA.new 2048 # key size
p1 = rsa.to_s # private key
p2 = rsa.public_key # public key
Base64::urlsafe_encode64(p1) # encoded_private_key
Base64::urlsafe_encode64(p2) # encoded_public_key

# In termial
heroku config:set RSA_PRIVATE_KEY=encoded_private_key
heroku config:set RSA_PUBLIC_KEY=encoded_public_key
```